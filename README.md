# This is an example project to create a gitlab Kubernetes runner for a GitLab
Please refer to official [documentation](https://docs.gitlab.com/runner/install/kubernetes.html)

##Basic steps:
 - Add helm repository:  
   helm repo add gitlab https://charts.gitlab.io
 - Upadate gitlab-runner-values.yaml:  
   Specify gitlab server url, registration token, cluster wide access, service account, name and tag for the runner
 - Create namespace for your runner:  
   kubectl create namespace gitlab-runner
 - Prepare cluster with service account creation and authorize it with permissions (Please carefully inspect permissions):  
   kubectl apply -f clusterPrepare.yaml
 - Deploy runner to your kubernetes:  
   helm install --namespace gitlab-runner gitlab-runner -f gitlab-runner-values.yaml gitlab/gitlab-runner

##Runner unregistering
 - Get runner token (attach to runners pod):  
   gitlab-runner list
 - Unregister runner:  
   gitlab-runner unregister --url https://gitlab.com/ --token <token>